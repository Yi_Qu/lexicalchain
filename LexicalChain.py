from Sentences import Sentences
from NpIdentifier import NpIdentifier
from nltk.corpus import wordnet as wn
import pandas as pd


class LexicalChain:
    def __init__(self, filename):
        self.filename = filename
        self.lexicalChains = []
        self.lcSentences = []  # List of sentences IDs in LC
        self.similarityChains = []  # Lexical Chain based on similarity
        self.Dict = {}  # Dictionary of nouns and synonyms
        self.CorpusSentences = Sentences(filename)
        self.sentences = self.CorpusSentences.sentences  # Parsed Sentences
        self.text = self.CorpusSentences.text  # Original text
        self.nps = NpIdentifier(self.sentences, self.text)
        self.words = self.nps.words
        self.nouns = self.nps.nouns
        self.pronouns = self.nps.pronouns
        self.nounSenses = self.nps.nounSenses
        self.mentionList = self.nps.mentionList  # List of Named Entities
        self.buildLexicalChain()
        self.checker()
        self.CorpusSentences.buildSummary(self.lexicalChains, self.lcSentences,
                                          self.mentionList, self.nps.menDict)

    def buildLexicalChain(self):
        '''Scan each word and compare in Dict'''
        self.updateDict(self.nouns[0][0], 0, 0)
        sid = 0
        for sentence in self.nouns:
            wid = 0
            for word in sentence:  # For every word
                print(word)
                self.updateLC(word, sid, wid)
                self.updateSC(word)
                self.Dict[word][-1] += 1
                wid += 1
            sid += 1

    def updateLC(self, word, sid, wid):
        '''updage Lexical Chain'''
        flag = 0
        for j in range(len(self.lexicalChains)):
            for lcWord in self.lexicalChains[j]:  # For every word in LC
                if(self.isSynonym(word, lcWord.split("(")[0], sid, wid)):
                    toAdd = word + str("("+str(self.Dict[word][-1])+str(")"))
                    self.lexicalChains[j] += [toAdd]
                    self.lcSentences[j] += [sid]
                    flag = 1
                    break
            else:
                continue
            break
        if(flag == 0):
            tmpstr = word + str("(" + str(self.Dict[word][-1]) + str(")"))
            self.lexicalChains.append([tmpstr])
            self.lcSentences.append([sid])

    def updateSC(self, word):
        flag = 0
        for j in range(len(self.similarityChains)):
            for lcWord in self.similarityChains[j]:
                if(self.compareSimilarities(word, lcWord.split("(")[0])):
                    toAdd = word + str("("+str(self.Dict[word][-1])+str(")"))
                    self.similarityChains[j] += [toAdd]
                    flag = 1
                    break
            else:
                continue
            break
        if(flag == 0):
            tmpstr = word + str("(" + str(self.Dict[word][-1]) + str(")"))
            self.similarityChains.append([tmpstr])

    def isAntonym(self, n1, n2):
        if (n1 not in self.Dict):
            self.updateDict(n1)
        if (n2 not in self.Dict):
            self.updateDict(n2)
        # print("+++++++++++++++++++++")
        # print(self.Dict[n1][1])
        # print(self.Dict[n2][1])
        # print("=====================")
        if (n1 in self.Dict[n2][2] or n2 in self.Dict[n1][2]):
            return True
        else:
            return False

    def isSynonym(self, n1, n2, sid, wid):
        '''Check if n1 is the syno/mero/holo nyms'''
        if (n1 not in self.Dict):
            self.updateDict(n1, sid, wid)
        # if (n2 not in self.Dict):
        #     self.updateDict(n2)
        # print("+++++++++++++++++++++")
        # print(self.Dict[n1][0])
        # print(self.Dict[n2][0])
        # print("=====================")

        if (n1 in self.Dict[n2][0] or n2 in self.Dict[n1][0]):
            # Synonym
            return True
        elif (n1 in self.Dict[n2][1] or n2 in self.Dict[n1][1]):
            # Hypo Hypernyms
            return True
        elif (n1 in self.Dict[n2][2] or n2 in self.Dict[n1][2]):
            # Mero Holonyms
            return True
        elif (len(list(set(self.Dict[n1][1]) & set(self.Dict[n2][1]))) > 0):
            return True
        elif (len(list(set(self.Dict[n1][2]) & set(self.Dict[n2][2]))) > 0):
            return True
        else:
            return False

    def compareSimilarities(self, n1, n2):
        w1 = wn.synsets(n1)
        w2 = wn.synsets(n2)
        if(not w1 or not w2):
            return False
        sim = [i.wup_similarity(j) for i in w1 for j in w2]
        sim = pd.Series(sim).fillna(0).tolist()
        sim.sort()
        if(len(sim) > 0 and sim[-1] >= 0.7):
            return True
        else:
            return False

    def updateDict(self, n, sid, wid):
        if (len(self.nounSenses[sid]) > 0):
            wordSense = self.nounSenses[sid][wid]  # The word sense from lesk
        else:
            wordSense = wn.synsets(n)[0]  # Select 1st in Synsets
        values = []
        synonyms = [n]
        hypohyper = []
        meroholo = []
        antonyms = []
        # for ss in wn.synsets(n):
        for ss in [wordSense]:
            synonyms += ss.lemma_names()
            for hyper in ss.hypernyms():
                hypohyper += hyper.lemma_names()
            for hypo in ss.hyponyms():
                hypohyper += hypo.lemma_names()
            for pmero in ss.part_meronyms():
                meroholo += pmero.lemma_names()
            for smero in ss.substance_meronyms():
                meroholo += smero.lemma_names()
            for pholo in ss.part_holonyms():
                meroholo += pholo.lemma_names()
            for sholo in ss.substance_holonyms():
                meroholo += sholo.lemma_names()
            for lm in ss.lemmas():
                for anto in lm.antonyms():
                    antonyms.append(anto.name())
        synonyms = list(set(synonyms))
        values.append(synonyms)
        hypohyper = list(set(hypohyper))
        values.append(hypohyper)
        meroholo = list(set(meroholo))
        values.append(meroholo)
        antonyms = list(set(antonyms))
        values.append(antonyms)
        values.append(1)
        # print(values)
        self.Dict.update({n: values})  # Update dictionary

    def checker(self):
        print("Original text = ")
        print(self.sentences)
        self.printLC()
        self.printSC()

    def printLC(self):
        print("-------------Lexical_Chain---------------")
        k = 1
        for j in range(len(self.lexicalChains)):
            toPrint = "Chain " + str(k) + ": "
            for i in range(len(self.lexicalChains[j])):
                toPrint += str(self.lexicalChains[j][i] + ", ")
            toPrint = toPrint[:-2]
            print(toPrint)
            k += 1

    def printSC(self):
        print("-------------Similarity_Chain------------")
        k = 1
        for j in range(len(self.similarityChains)):
            toPrint = "Chain " + str(k) + ": "
            for i in range(len(self.similarityChains[j])):
                toPrint += str(self.similarityChains[j][i] + ", ")
            toPrint = toPrint[:-2]
            print(toPrint)
            k += 1


if __name__ == "__main__":
        lc = LexicalChain("input.txt")
