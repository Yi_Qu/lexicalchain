import spacy
import operator


class Sentences:
    def __init__(self, filename):
        self.text = []
        self.sentences = []
        self.senToLc = {}  # Map sentences with LCs
        if filename == "":
            filename = "input.txt"
        self.splitSentences(filename)

    def splitSentences(self, filename):
        nlp = spacy.load('en_core_web_sm')
        with open(filename) as f:
            for line in f:
                # self.sentences += nltk.sent_tokenize(line)
                self.text.append(line)
                doc = nlp(line)
                self.sentences += [str(i) for i in doc.sents]

    def getMentionsToSentences(self, mentionList):
        '''Map mentions to Sentences'''
        sid = mentionList[0]
        mid = 0
        ret = {}
        while(mid < len(mentionList)):
            if(mentionList[mid] == sid):
                ret.update({mid: sid})
            else:
                while(sid < len(self.sentences)-1):
                    sid += 1
                    if(mentionList[mid] == sid):
                        ret.update({mid: sid})
                        break
            mid += 1
        return ret

    def getInconsis(self, menToSents, coref):
        '''If any pair of selected sentences has overlapped mention'''
        inconsis = []
        for c in coref:
            temc = []
            for i in range(len(c)):
                temc += c[i]
            c = temc
            for j in coref:
                temj = []
                for k in range(len(j)):
                    temj += j[k]
                j = temj
                if c == j:
                    continue
                men1 = list(map(lambda x: menToSents[x], c))
                men2 = list(map(lambda x: menToSents[x], j))
                intersect = list(set(men1).intersection(set(men2)))
                if (len(intersect) == 0):
                    inconsis.append(intersect)
        return inconsis

    def corefSolver(self, menToSents, menDict, selSents):
        '''Solve the co-reference'''
        coref = []
        ret = []
        for sid in selSents:
            tmpCoref = []
            for i in menToSents:
                if(menToSents[i] == sid
                   and i in menDict
                   and len(menDict[i]) > 1):
                    tmpCoref += [menDict[i]]
                    # Select sentences with multi mentions
            coref.append(tmpCoref)
        inconsis = self.getInconsis(menToSents, coref)
        corefDict = {}  # Map mention with sentences
        for i in range(len(self.sorted_senToLc)):
            sid = self.sorted_senToLc[-i][0]
            if(sid in selSents):
                continue
            score = 0
            for inc in inconsis:
                if sid in inc:
                    score += 1
            corefDict.update({sid: score})
        corefDict = sorted(corefDict.items(), key=operator.itemgetter(1))
        ret = [corefDict[0][0]]
        return ret

    def buildSummary(self, lexicalChain, lcSentences, mentionList, menDict):
        print("_________SUMMARIZATION_________")
        upperBound = len(self.text[0]) // 4 + 1
        menToSents = self.getMentionsToSentences(mentionList)
        senToLc = {}
        senID = 0
        while(senID < len(self.sentences)):
            occur = 0
            lcID = 0
            while(lcID < len(lcSentences)):
                if(senID in lcSentences[lcID]):
                    occur += 1
                lcID += 1
            senToLc.update({senID: occur})
            senID += 1
        self.sorted_senToLc = sorted(senToLc.items(),
                                     key=operator.itemgetter(1))
        numOfSents = 0
        summary = ['']
        ss = []
        addition = []
        while (len(summary[0]) <= upperBound):
            summary[0] = ''
            numOfSents += 1
            sumSents = self.sorted_senToLc[-numOfSents:]
            ss = [x[0] for x in sumSents]
            addition = self.corefSolver(menToSents, menDict, ss)
            #   The above expression implement coref resolution
            #   You can uncomment it and run the program again
            #   Sometimes it doesn't word
            sorted_ss = sorted(list(set(ss)))
            for i in sorted_ss:
                summary[0] += self.sentences[i]
                summary[0] += ' '
        if (len(addition) > 0):
            summary[0] = ''
            sorted_ss = sorted(list(set(ss + addition)))
            for i in sorted_ss:
                summary[0] += self.sentences[i]
                summary[0] += ' '
        print(summary)


if __name__ == "__main__":
    print("in Sentences")
